#!/usr/bin/env bash

# Second try at building a script to setup my system for me
# This one priortizes certain programs and files first to get a semi-working machine up as soon as possible.

# Parse arguments using getopt
args=$(getopt -n setup-system -o d --long "debug" -- "$@")
if [ $? -ne 0 ]; then
    exit 1
fi
eval set -- "$args"

while true; do
    case "$1" in
        -d)
            # Debug mode (Don't redirect stdout off of the terminal)
            DEBUG=true
            shift
            ;;

        --)
            shift
            break
            ;;

        *)
            echo "Bad getopt config" >&2
            exit 10
            ;;

	  esac
done

######

# Create log files
mkdir -p ~/logs
logfile=~/logs/"setup-system-$(date '+%Y%m%d%H%M%S')"
touch $logfile

# Redirect all stdout to logfile, and put stderr on both terminal and in logfile
if [ -z "$DEBUG" ]; then
    exec 3>&1
    exec > >(tee -a ${logfile} >/dev/null) 2> >(tee -a ${logfile} >&3)
fi


# Main setup code
function main {    
    
    msg "Step 0: Get preferences"
    get-preferences
    sudo apt-get update
    
    if [ "$STARTSTEP" -eq "1" ]; then
        msg "Step 1: Connect to central server"
        connect-to-server
    fi
    
    if [ "$STARTSTEP" -eq "2" ]; then
        msg "Step 2: Sync up most essential files"
        install-unison
        sync-essentials
        inst stow
        mkdir -p ~/.dotfiles/$(hostname)
        interlink-files
        timezone-sync
    fi
    
    if [ "$STARTSTEP" -eq "3" ]; then
        msg "Step 3: Setup terminal"
        install-zsh
        install-terminals
    fi
    
    if [ "$STARTSTEP" -eq "4" ]; then
        msg "Step 4: Install essential programs" 
        install-essentials
        install-taskwarrior
    fi
    
    if [ "$STARTSTEP" -eq "5" ]; then
      msg "Step 5: Sync rest of files & install other programs"
      synchronize &
      install-other
    fi

    msg "Done. Enjoy your new computer"
    
}

# Define useful shorthand commands
function inst {
    echo -e "Installing $*" >&2
    sudo apt-get install -y $*
    if [ $? -ne 0 ]; then
        echo -e "$(tput setaf 1)\n!!! ERROR !!!\n$(tput sgr0)\n" >&2
    fi
}

function headinst {
    # funtion that installs a program only if using a machine with a display
    if [ -z "$HEADLESS" ]; then
        inst $*
    fi
}

function msg {
    echo -e "\n\n$(tput bold)$*$(tput sgr0)" >&2
    echo -e "----------------\n" >&2
}


function get-preferences {
    # Is machine headless
    echo -en "Is this machine headless? [y/N]:" >&2
    read
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        HEADLESS=true
    fi
    
    # Sync capacity of machine
    echo -e "
    Sync Depth:
    0) Essentials [ only bin, Data, Analysis, Finances, Notebooks, .dotfiles, Protocols ]
    1) UltraLight [ no Videos, TollingBell, Music, Phone, Media, Mail, School Documents ]
    2) Light      [ no Videos, TollingBell, Phone ]
    3) Full" >&2
    
    while true; do
        read -p "[0-3]:" SYNCDEPTH
        
        case $SYNCDEPTH in
        
            0)
                SYNCNAME=Essentials
                break
                ;;
                
            1)
                SYNCNAME=UltraLightSync
                break
                ;;
            
            2)
                SYNCNAME=LightSync
                break
                ;;
            
            3)
                SYNCNAME=Synchronize
                break
                ;;
            
            *)
                continue
                ;;
        
        esac
    done
                
            
    until [ "$SYNCDEPTH" -ge "0" -a "$SYNCDEPTH" -le "3" ] &>/dev/null; do
        read SYNCDEPTH
    done
    
    echo -e "
    What would you like to do?
    
    1) Connect to central server
    2) Sync up most essential files
    3) Setup terminal
    4) Install essential programs (including taskwarrior)
    5) Sync rest of files & install other programs
    " >&2
    
    until [ "$STARTSTEP" -ge "1" -a "$STARTSTEP" -le "5" ] &>/dev/null; do
        read -p "[1-5]:" STARTSTEP
    done
    
}

function connect-to-server {
    # Make an ssh key if one does not already exist
    if [ ! -e ~/.ssh/id_rsa ]; then
        ssh-keygen -f ~/.ssh/id_rsa -N ''
    fi
    
    # Copy over the identity to the server
    ssh-copy-id -o StrictHostKeyChecking=no fabernj@s.njfaber.com
    
    # Start the ssh service
    inst openssh-server
    sudo service ssh start

    # Make ControlMaster socket folder
    mkdir ~/.ssh/cm_socket
    
}

function install-unison {
#     # Purge any existing unison versions
#     sudo apt-get purge -y unison
#     sudo rm /usr/bin/unison*
#     
#     # Get unison package (for consistent versioning) off the main server
#     scp fabernj@s.njfaber.com:./Backups/Packages/unison.deb /tmp/unison.deb
#     
#     # Install it
#     sudo dpkg -i /tmp/unison.deb
#     inst -f
#     
#     # Prevent upgrades
#     sudo apt-mark hold unison
#     
#     # Clean up
#     rm /tmp/unison.deb
    inst unison
}

function sync-essentials {
    # Copy over "Essentials.prf" which tells unison the things to sync
    mkdir -p ~/.unison
    scp fabernj@s.njfaber.com:./.unison/Essentials.prf ~/.unison/.
    
    # Sync up
    unison -auto -batch -prefer ssh://fabernj@s.njfaber.com/./Synchronize Essentials
}

function interlink-files {
    ~/Synchronize/bin/interlink -f
}

function timezone-sync {
    sudo ln -sf ~/.localtime /etc/localtime
    sudo ln -sf ~/.timezone /etc/timezone
}

function install-zsh {
    inst zsh
    inst git
    
    # Install oh-my-zsh
    rm -rf ~/.oh-my-zsh
    # Use a different umask to prevent compdef from failing
    (umask g-w,o-w && env git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh)
    echo 'ZSH_THEME="agnoster"' > ~/.zshtheme
    
    # Change the shell to zsh
    chsh -s $(which zsh)
}
    
function install-terminals {
    headinst yakuake
    headinst terminator
    inst tmux
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
}

function install-taskwarrior {
    # Install task warrior
    if [[ $(task --version) != 2.5.1 ]]; then
        mkdir -p ~/src
        cd ~/src
        git clone https://github.com/GothenburgBitFactory/taskwarrior
        cd taskwarrior
        git checkout v2.5.1
        inst make cmake clang gcc libuuid1 gnutls-bin libreadline-dev libgnutls28-dev uuid-dev
        cmake -DCMAKE_BUILD_TYPE=release .
        make
        sudo make install
        cd
    fi
    
    echo -n 'recurrence=no' > ~/.tasklocal
    echo -n 'context=home' > ~/.context
}

function install-essentials {
    
    inst entr
    
    # Browser
    headinst firefox
    
    # Email
    headinst thunderbird
    
    headinst redshift
    
    # Text editors
    inst vim
    mkdir -p ~/.vim ~/.vim/_undo ~/.vim/_backup ~/.vim/_swap 
    
    # Install spacemacs
	inst emacs
	git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
	
	inst expect
	
	inst inotify-tools
	inst notify-osd
	inst libnotify-bin
	
	inst python
	inst python3
	inst ruby
	
	sudo gem install mdless
  sudo gem install tmuxinator
	
	inst curl
	inst wget
	
	inst toilet
	inst figlet
	inst ledger
    
    inst gcalcli
}

function synchronize {
    # Incrementally synchronize files up to desired sync depth
    
    # First add the proper sync depth to /etc/environment, so that when sync is called the right depth is used
    export SYNCNAME
    sudo sh -c "echo SYNCNAME=$SYNCNAME >> /etc/environment"
    
    # Now incrementally sync each level up
    if [ "$SYNCDEPTH" -gt 0 ]; then
        msg "Performing UltraLightSync"
        unison -auto -batch -prefer ssh://fabernj@s.njfaber.com/./Synchronize UltraLightSync
    fi
    
    if [ "$SYNCDEPTH" -gt 1 ]; then
        msg "Performing LightSync"
        unison -auto -batch -prefer ssh://fabernj@s.njfaber.com/./Synchronize LightSync
    fi
    
    if [ "$SYNCDEPTH" -gt 2 ]; then
        msg "Performing Full Sync"
        unison -auto -batch -prefer ssh://fabernj@s.njfaber.com/./Synchronize Synchronize
    fi

}
function install-other {
	# Install all the other programs
	# TODO: migrate most of this to a sepearte file that can be added to easily
	
	# Add repositories
    sudo apt-get update
    
	headinst gparted
	headinst rhythmbox-plugins
	headinst vlc
	headinst inkscape
	headinst feh
	headinst imagemagick
	headinst retext
	headinst audacity
	headinst regexxer
	headinst konsole
	headinst qbittorrent
	headinst kmix
	headinst calibre
	headinst kdiff3
	headinst rhythmbox
	headinst krusader
	headinst typecatcher
	
	inst coreutils
	inst pandoc
	inst qalc
	inst ncdu
	inst openvpn
	inst jq
	inst pv
	
	# if ! hash electrum; then
    #     pip3 install https://download.electrum.org/3.0/Electrum-3.0.tar.gz
    # fi
	
	## Programming languages
    inst ruby ruby-dev ruby-full
    inst python python-dev python-pip python-ipython python-numpy python-scipy
    inst python3 python3-dev python3-pip python3-pyqt5 python3-tk
    inst julia
    inst octave
	inst r-base r-base-dev
	
	# Download and Install RStudio
    # wget https://download1.rstudio.org/rstudio-xenial-1.1.383-amd64.deb
    # dpkg -i rstudio-xenial-1.1.383-amd64.deb
    # $inst -f
    # rm rstudio-xenial-1.1.383-amd64.deb
	
	## pips, gems, etc
    sudo pip3 install numpy scipy matplotlib ipython jupyter pandas sympy nose plotly cufflinks
    sudo pip3 install thefuck
	sudo gem install bundler
    sudo gem install bibtex-ruby
    sudo gem install curses ncurses
    sudo gem install mdless
    sudo gem install bropages
    
    # Install steam
    # if ! hash steam; then
    #    cd /tmp
    #    wget -E http://media.steampowered.com/client/installer/steam.deb && \
    #    dpkg -i steam.deb; \
    #    $inst -f
    #    cd
    # fi
}

main
